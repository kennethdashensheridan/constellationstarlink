package parsing

// constellationResponse is a struct that contains the response format from the Starlink constellation API.
type constellationResponse struct {
	SpaceTrack struct {
		CCSDSOMMVERS       string  `json:"CCSDS_OMM_VERS"`
		COMMENT            string  `json:"COMMENT"`
		CREATIONDATE       string  `json:"CREATION_DATE"`
		ORIGINATOR         string  `json:"ORIGINATOR"`
		OBJECTNAME         string  `json:"OBJECT_NAME"`
		OBJECTID           string  `json:"OBJECT_ID"`
		CENTERNAME         string  `json:"CENTER_NAME"`
		REFFRAME           string  `json:"REF_FRAME"`
		TIMESYSTEM         string  `json:"TIME_SYSTEM"`
		MEANELEMENTTHEORY  string  `json:"MEAN_ELEMENT_THEORY"`
		EPOCH              string  `json:"EPOCH"`
		MEANMOTION         float64 `json:"MEAN_MOTION"`
		ECCENTRICITY       float64 `json:"ECCENTRICITY"`
		INCLINATION        float64 `json:"INCLINATION"`
		RAOFASCNODE        float64 `json:"RA_OF_ASC_NODE"`
		ARGOFPERICENTER    float64 `json:"ARG_OF_PERICENTER"`
		MEANANOMALY        float64 `json:"MEAN_ANOMALY"`
		EPHEMERISTYPE      int     `json:"EPHEMERIS_TYPE"`
		CLASSIFICATIONTYPE string  `json:"CLASSIFICATION_TYPE"`
		NORADCATID         int     `json:"NORAD_CAT_ID"`
		ELEMENTSETNO       int     `json:"ELEMENT_SET_NO"`
		REVATEPOCH         int     `json:"REV_AT_EPOCH"`
		BSTAR              float64 `json:"BSTAR"`
		MEANMOTIONDOT      float64 `json:"MEAN_MOTION_DOT"`
		MEANMOTIONDDOT     float64 `json:"MEAN_MOTION_DDOT"`
		SEMIMAJORAXIS      float64 `json:"SEMIMAJOR_AXIS"`
		PERIOD             float64 `json:"PERIOD"`
		APOAPSIS           float64 `json:"APOAPSIS"`
		PERIAPSIS          float64 `json:"PERIAPSIS"`
		OBJECTTYPE         string  `json:"OBJECT_TYPE"`
		RCSSIZE            string  `json:"RCS_SIZE"`
		COUNTRYCODE        string  `json:"COUNTRY_CODE"`
		LAUNCHDATE         string  `json:"LAUNCH_DATE"`
		SITE               string  `json:"SITE"`
		DECAYDATE          string  `json:"DECAY_DATE"`
		DECAYED            int     `json:"DECAYED"`
		FILE               int     `json:"FILE"`
		GPID               int     `json:"GP_ID"`
		TLELINE0           string  `json:"TLE_LINE0"`
		TLELINE1           string  `json:"TLE_LINE1"`
		TLELINE2           string  `json:"TLE_LINE2"`
	} `json:"spaceTrack"`
	Launch      string      `json:"launch"`
	Version     string      `json:"version"`
	HeightKm    interface{} `json:"height_km"`
	Latitude    interface{} `json:"latitude"`
	Longitude   interface{} `json:"longitude"`
	VelocityKms interface{} `json:"velocity_kms"`
	Id          string      `json:"id"`
}
