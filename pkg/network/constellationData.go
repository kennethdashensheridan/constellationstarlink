package network

import (
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
)

func GetConstellationData() {
	fmt.Println("1. Performing Http Get...")
	resp, err := http.Get("https://api.spacexdata.com/v4/starlink/")
	if err != nil {
		log.Fatal(err)
	}

	defer resp.Body.Close()
	bodyBytes, err := ioutil.ReadAll(resp.Body)

	// Convert byte array to string
	bodyString := string(bodyBytes)
	fmt.Println("API Response as String:\n", bodyString)

	/*// Convert byte array to Todo struct
	var todoStruct Todo
	json.Unmarshal(bodyBytes, &todoStruct)
	fmt.Printf("API Response as Todo struct %+v\n", todoStruct)*/
}
