BINARY_NAME=constellationStarlink

.DEFAULT_GOAL := build

fmt:
		go fmt ./...
.PHONY:fmt

lint: fmt
		golint ./...
.PHONY:lint

vet: fmt
		go vet ./...
.PHONY:vet

run: build
		go run -race main.go
.PHONY:run

build:vet
		GOARCH=amd64 GOOS=darwin go build -o ${BINARY_NAME}-macOS main.go
		GOARCH=amd64 GOOS=linux go build -o ${BINARY_NAME}-linux main.go
.PHONY:build
