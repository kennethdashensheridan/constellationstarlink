/*
Copyright © 2022 NAME HERE <EMAIL ADDRESS>

*/
package main

import "gitlab.com/kennethdashensheridan/constellationstarlink/cmd"

func main() {
	cmd.Execute()
}
